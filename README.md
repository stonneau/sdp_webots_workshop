# SDP_Webots_workshop


The objective for this workshop is first to make sure you manage to run the Webots simulator 
correctly. Secondly, it aims at getting you familiar with the simulator. This workshop is heavily based on the available material and resources available in the [Webots documentation and tutorials](https://cyberbotics.com/doc/guide/tutorials).

Specifically, we will see how to load the robots used in the SDP class, setup a basic environment and write a controller
for your robot.

For each of these tasks, during the workshop your instructor will go over the relevant tutorial with you. You will be then
required to adapt the instructions to your model.

The present file describes how to install / run the Webots simulator. **You should have completed all steps indicated
below prior to the workshop !** Try to test your installation at least 2 days before the workshop. This repository will be updated with the workshop content on the day before the workshop.

# Installation (prior to the workshop)

You can either install Webots locally on your machine, or run it on a DICE environment remotely. 
We strongly encourage you to install Webots locally as the Graphical applications run really slowly remotely.
**Only members of the team who have the simulator running locally should participate.**

## local Installation on your OS (recommended)
For the workshop, You can install [Webots](https://cyberbotics.com/) on either a Mac / Linux / Windows OS machine.

Prior to installing Webots, you should also configure your programming environment.
In the workshop we will work with **Python**, for which the instructions are provided [here](https://cyberbotics.com/doc/guide/using-python).
See [https://cyberbotics.com/doc/guide/language-setup](https://cyberbotics.com/doc/guide/language-setup) for instructions on all langagues.

The installation should be straightforward. Simply download the archive and run the installer as advised [here](https://cyberbotics.com/doc/guide/installation-procedure).


## remote access to University machines (**not** recommended)
If you have any other option you should not try to run Webots remotely.
As Tim Colles mentioned in his email Graphical applications run slowly when DICE machines are accessed remotely.
You will be able to launch Webots and program anything you need using remote access, but the simulation will run excessively slowly.
This solution should only be used if no other option is available. For the workshop, you should send members from your team 
for which a local installation is possible. For your project your team should be able to configure a streaming server that would allow
the simulator to [run on a web browser](https://cyberbotics.com/doc/guide/web-simulation). We are still working on a global solution with IT to this issue.

For remote access to DICE machine please consult [this page](http://computing.help.inf.ed.ac.uk/remote-desktop)

## Test the installation

### Launch Webots
- Linux: Open a terminal and type `webots` to launch Webots.
- macOS: Open the directory in which you installed the Webots package and double-click on the Webots icon.
- Windows: open the Start menu, go to the Program Files / Cyberbotics menu and click on the Webots R2021a revision 1 menu item.

You can find more details on the execution of the software on the [dedicated manual page](https://cyberbotics.com/doc/guide/starting-webots)

### Open any example simulation
The first time you launch Webots you will be proposed a guided tour. You can simply browse through any sample demonstration and start
it to make sure everything is running correctly.
