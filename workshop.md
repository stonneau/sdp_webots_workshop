# Webots hands-on tutorial

## Introduction and scope

The objective of this workshop is for you to get a hands-on experience with Webots using the Turtlebot robot, which is one possible
option for the SDP class.

This workshop is heavily based on the available material and resources available in the [Webots documentation and tutorials](https://cyberbotics.com/doc/guide/tutorials) and will specifically point to sections of the tutorial in the first part. You are encouraged to follow all the tutorials on your own to complete your training with Webots.

We will see how to use Webots to:
- Create a project and a simple scene
- Populate the scene with obstacles and a Robot
- Implement a controller that allows the Turtlebot to move following a straight line
- Implement a more advanced controller that uses sensor information to orient the Turtlebot towards the closest obstacles.

The tutorial is designed as a series of questions, which will require you to look for relevant information in the documentation.
Additional explanations will be provided during the class, but by referring to the tutorials and documentation you should be able to achieve it by yourselves.

We will use Python as the main language for programming the controller. However you should feel free to use any supported language.




## Create a environment

Your first task will be to create an empty environment and populate it with a floor and some obstacles.
Follow hands-on #1, #2 and #3 from [Webots tutorial 1](https://cyberbotics.com/doc/guide/tutorial-1-your-first-simulation-in-webots#create-a-new-world).

Then, proceed to these modifications:

1. Change the arena `floorSize` to obtain a 2 meters by  2 meters square arena.
2. Change the wallHeight to 1 meter

Your world should look like this:

[![IMAGE ALT TEXT](images/arena1.png)]()

## Import the Turtlebot model

One of the available robot for the SDP course is the [Turtlebot3 robot burger](https://emanual.robotis.com/docs/en/platform/turtlebot3/overview/). It is a mobile robot with two wheels, each controled by a motor and comes equiped with a 2D Lidar sensor. We will first import the robot to the scene and write a simple controller to make it move.


1. Click on the RectangleArena object in the scene tree view, then click on the Add button <img src="/images/add-button.png"  width="26" height="26">

2.  In the dialog box, choose `PROTO nodes (Webots Projects) / robots / robotis / Turtlebot` and finally select `TurtleBot3Burger (Robot)` and click on `Add`.

3. The corresponding node should have been added to the scene tree view and you should now be able to visualise the TurtleBot

4. Save your world using `Ctrl + Shift + S` and run the simulation

5. Nothing happens ! That's because no behaviour has been specified for our robot.

## Make the Turteblot move

For the TurtleBot to move, we need to implement a controller that will send command to the motors at each step of the simulation.
Let's look at the model specification for the TurtleBot and identify the motors of interest.

1. Stop the simulation by clicking on <img src="/images/pause-button.png"  width="26" height="26"> and reset it by clicking on <img src="/images/reset-simulation-button.png" width="26" height="26">

2. In the scene tree view, right-click on the `TurtleBot3Burger` node, then on `View PROTO source`. On the text editor on the right of your window, you should now see the content of a file called `TurtleBot3Burger.proto`. proto files are used to describe elements of the world in Webots. You can find more information on proto files in the [documentation](https://cyberbotics.com/doc/reference/proto?tab-language=python). You might also want to follow [tutorial 7](https://cyberbotics.com/doc/guide/tutorial-7-your-first-proto?tab-language=python) to create your own proto file. For the moment, we are simply interested in identifiying the motors name. Look for the occurences of the `RotationalMotor` keyword and make note of the associated names for the two motors of the TurtleBot: "**right wheel motor**" and "**right wheel motor**". You can also notice that the maximum velocity for each motor is **6.67**. This velocity is expressed in radians per seconds.

3. Now write your first controllers, following [hands-on #8, #9 and #10](https://cyberbotics.com/doc/guide/tutorial-1-your-first-simulation-in-webots?tab-language=python#create-a-new-controller). You will of course adapt the steps to the TurtleBot rather than the epuck and use the parameters gathered in question 2. You are free to choose any language for implementing
your controller though **Python** is recommended.

4. (Optional) Update your controller code in such a way that the robot rotates (more or less) in place, that is around its center without translating.

## Working with sensors
To act on the world, robots need to perceive their environment somehow. The Turteblot3 is delivered with 
a [2D Laser scanner](https://en.wikipedia.org/wiki/Lidar) that allows it to measure the distance between the laser and the closest obstacles in all directions. We will use the simulated Lidar in Webots to write a controller that interactively rotates the robot to face the closest obstacle.

1. Reset your simulation.

2. Add obstacles to the scene. Follow [hands-on #4](https://cyberbotics.com/doc/guide/tutorial-1-your-first-simulation-in-webots?tab-language=python#create-a-new-world) to add a 3 wooden boxes to the scene. Change their height to 0.4. Save the scene.

3. Double-click on the `TurtleBot3Burger` node, then double-click on `extensionSlot` to reveal the Lidar device.
Right-click on `RobotisLds01 "LDS-01" / View generated PROTO Node` and take note of the name of the first rotational motor.

4. Enable the Lidar. Use the ``getDevice` method to retrieve an instance of the Lidar. Browse the [Lidar API](https://cyberbotics.com/doc/reference/lidar?tab-language=python) to find how to enable the Lidar. Use the simulation timestep for determining the sampling time.

5. Each time robot.step will be called, the lidar reading will be updated and its value available in the form of an array, obtained 
by calling the method [`Lidar.getRangeImage`](https://cyberbotics.com/doc/reference/lidar?tab-language=python#wb_lidar_get_range_image).
This method returns a python list of floating point values, each containing the minimum distance to an obstacle in one direction, represented by the blue rays in this picture:

<img src="/images/lidar.png"  height="250">


6. Take a look at the [Lidar specification](https://www.robotis.us/360-laser-distance-sensor-lds-01-lidar/) to find the following information: 
    - The number of rays (deduced from the angular range and resolution). Check your answer by measuring the size of the list returned by `Lidar.getRangeImage`
    - The distance range

7. Update your controller to continuously print the value of the first entry of the array returned by `Lidar.getRangeImage`. Save the controller
and run the simulation. Interactively move one of the wooden box around the robot and see how the value changes. Which direction is the first ray facing ? What happens when the obstacle gets too close from the sensor ?

8. Program a controller that will drive the Turteblot to face the closest object.
Don't forget to handle too close objects

9. Save your world and reset your simulation. Lower the arena walls to 0.1 cm and remove all obstacles from the scene.
What value is return by the Lidar sensor when no obstacles are in the range ?

10. (Optional) Just for visualisation, you can animate the lidar secondary motors (this does not have any effect on the simulation).
Identify the motors in the lidar proto file and give them an appropriate velocity.
