#!/usr/bin/env python3

"""default_controller controller."""
#Standard python libraries import
from numpy import inf


# You may need to import some classes of the controller module. Ex:
#  from controller import Robot, Motor, DistanceSensor
from controller import Robot

### Some given variables for the workshop ### 
TODO = None # TODO indicates where in the code you should add your input


TIME_STEP = 64  #default time step in ms
BASE_SPEED = 6.5 # maximum velocity of the turtlebot motors


### Helper functions to initialise the turtlebot motor devices ###
def initRobot():
    """Create a robot instance, retrieve the  """
    robot = Robot()
    leftMotor  = robot.getDevice('left wheel motor')
    rightMotor = robot.getDevice('right wheel motor')
    return robot, leftMotor, rightMotor

def initLidar():
    """Retrieve the lidar Device, activate it and return the lidar instance"""
    lidar = robot.getDevice('LDS-01')
    lidar.enable(TIME_STEP)
    return lidar


def enableSpeedControl(motor, initVelocity = 0.):
    """Disable position control and set velocity of the motor to initVelocity (in rad / s)"""
    motor.setPosition(float('inf'))  
    motor.setVelocity(initVelocity)  

def visualizeLidarRotation():
    """ get lidar motor and enable rotation (only for visualization, no effect on the sensor) """
    lidarMainMotor = robot.getDevice('LDS-01_main_motor')
    lidarSecondaryMotor = robot.getDevice('LDS-01_secondary_motor')
    enableSpeedControl(lidarMainMotor, 30.)
    enableSpeedControl(lidarSecondaryMotor, 60.)
 

### Main ###

#first initialise the robot and lidar controllers
robot, leftMotor, rightMotor = initRobot()
lidar = initLidar()

enableSpeedControl(leftMotor)
enableSpeedControl(rightMotor)

visualizeLidarRotation()

# Main loop:
# - perform simulation steps until Webots is stopping the controller
while robot.step(TIME_STEP) != -1:
    lidarImage = lidar.getRangeImage()
    for i, el in enumerate(lidarImage):
        if el == inf or el == -inf:
            lidarImage[i] = 0.1
    iDx = lidarImage.index(min(lidarImage))
    diff = 180 - iDx
    if (abs(diff)) < 5:
        leftMotor.setVelocity(0)
        rightMotor.setVelocity(0)
    elif iDx > 0:
        leftMotor.setVelocity(-float(diff) / 180. * BASE_SPEED )
        rightMotor.setVelocity(float(diff) / 180. * BASE_SPEED )
    else:        
        rightMotor.setVelocity(-float(diff) / 180. * BASE_SPEED)
        leftMotor.setVelocity(float(-diff) / 180. * BASE_SPEED)
    pass
