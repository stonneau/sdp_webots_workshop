#!/usr/bin/env python3

from controller import Robot, Motor

TIME_STEP = 64
BASE_SPEED = 6.5

# create the Robot instance.
robot = Robot()

# get the motor devices
leftMotor = robot.getDevice('left wheel motor')
rightMotor = robot.getDevice('right wheel motor')

def enableSpeedControl(motor, initVelocity = 0.):
    """Disable position control and set velocity of the motor to initVelocity (in rad / s)"""
    motor.setPosition(float('inf'))  
    motor.setVelocity(initVelocity)  


enableSpeedControl(leftMotor, BASE_SPEED)
enableSpeedControl(rightMotor,BASE_SPEED)

while robot.step(TIME_STEP) != -1:
   pass